import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.scss']
})
export class AddItemComponent implements OnInit {

  recipeName = '';
  recipeCount = 0;
  recipeList: any = [];
  odd = false;
  even = false;

  constructor() { }

  ngOnInit() {
  }

  onAddRecipe() {
    this.isOdd();
    this.recipeList.push({ rName: this.recipeName, rCount: this.recipeCount, isOdd: this.odd });
    this.recipeName = '';
    this.recipeCount = 0;
  }

  isOdd() {
    if (this.recipeCount % 2 !== 0) {
      this.odd = true;
      this.even = false;
    } else {
      this.odd = false;
      this.even = true;
    }
  }
}
